﻿using cadClientes.Control;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cadClientes.Model
{
    class BeansUsuario
    {
        int id;
        String login, senha, funcao;
        UsuarioDAO controle = new UsuarioDAO();
        public String getLogin()
        {
            return this.login;
        }
        public void setLogin(String login)
        {
            this.login = login;
        }
        public String getSenha()
        {
            return this.senha;
        }
        public void setSenha(String senha)
        {
            this.senha = senha;
        }
        public String getFuncao()
        {
            return this.funcao;
        }
        public void setFuncao(String funcao)
        {
            this.funcao = funcao;
        }
        public int getId()
        {
            return this.id;
        }
        public void setId(int id)
        {
            this.id = id;
        }
        public void criar()
        {
            controle.criar(this);
        }
        public BeansUsuario buscar()
        {
            BeansUsuario user = controle.Buscar(this);
            return user;
        }
        public void deletar()
        {
            controle.deletar(this);

        }
        public void atualizar()
        {
            controle.atualizar(this);
        }
    }
}
