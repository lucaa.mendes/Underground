﻿using cadClientes.Control;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cadClientes.Model
{
    class BeansCliente
    {
        int id;
        String nome = null, sobrenome = null, cpf, rg, email, telefone;
        ClienteDAO controle = new ClienteDAO();
        public void setId(int id)
        {
            this.id = id;
        }
        public int getId()
        {
            return this.id;
        }
        public void setNome(String nome)
        {
            this.nome = nome;
        }
        public String getNome()
        {
            return this.nome;
        }
        public void setSobrenome(String sobrenome)
        {
            this.sobrenome = sobrenome;
        }
        public String getSobrenome()
        {
            return this.sobrenome;
        }
        public void setCpf(String cpf)
        {
            this.cpf = cpf;
        }
        public String getCpf()
        {
            return this.cpf;
        }
        public void setRg(String rg)
        {
            this.rg = rg;
        }
        public String getRg()
        {
            return this.rg;
        }
        public void setEmail(String email)
        {
            this.email = email;
        }
        public String getEmail()
        {
            return this.email;
        }
        public void setTelefone(String telefone)
        {
            this.telefone = telefone;
        }
        public String getTelefone()
        {
            return this.telefone;
        }
        public void criar()
        {
            controle.criar(this);
        }
        public BeansCliente buscar()
        {
            BeansCliente user = controle.Buscar(this);
            return user;
        }
        public void deletar()
        {
            controle.deletar(this);
        }
        public void atualizar()
        {
            controle.atualizar(this);
        }
    }
}
