﻿using cadClientes.Model;
using cadClientes.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cadClientes
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        private void btnEntrar_Click(object sender, EventArgs e)
        {
            BeansUsuario user = new BeansUsuario();
            user.setLogin(txtLogin.Text);
            user.setSenha(txtSenha.Text);
            BeansUsuario userR = new BeansUsuario();
            userR = user.buscar();
            if(txtLogin.Text =="" || txtSenha.Text == "")
            {
                MessageBox.Show("Por favor preencher os campos obrigatorios");
            }
            else
            {
                if (user.getSenha() == userR.getSenha())
                {
                    this.Hide();
                    Form f = new Main();
                    f.Closed += (s, args) => this.Close();
                    f.Show();


                }
                else
                {
                    MessageBox.Show("Falha no Login");
                }
            }

        }
    }
}
