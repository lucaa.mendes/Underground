﻿using cadClientes.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cadClientes.Control
{
    class UsuarioDAO
    {
        public void criar(BeansUsuario usu)
        {
            SqlCommand cmd = null;
            SqlConnection con = null;
            string connectionString = @"Server=(localdb)\MSSQLLocalDB;Database=dbcrud;Trusted_Connection=True;";
            string sql = "INSERT INTO USUARIO(login,senha,funcao) VALUES ('" + usu.getLogin() + "','" + usu.getSenha() +"','"+usu.getFuncao()+"')";
            con = new SqlConnection(connectionString); 
            cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.Text;
            con.Open();
            try
            {
                int i = cmd.ExecuteNonQuery();
                if (i > 0)
                {
                    MessageBox.Show("Cadastro realizado com sucesso!");
                }
            } catch (Exception ex) {
                MessageBox.Show("Erro: " + ex.ToString());
            }
            finally {
                con.Close();
            }


        }
        public BeansUsuario Buscar(BeansUsuario usu)
        {
            BeansUsuario usuario = new BeansUsuario();
            SqlCommand cmd = null;
            SqlConnection con = null;
            string connectionString = @"Server=(localdb)\MSSQLLocalDB;Database=dbcrud;Trusted_Connection=True;";
            string sql = "SELECT * FROM USUARIO WHERE login = '" + usu.getLogin()+"'";
            con = new SqlConnection(connectionString);
            cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            con.Open();
            try
            {
                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    usuario.setId((int)reader[0]);
                    usuario.setLogin(reader[1].ToString());
                    usuario.setSenha(reader[2].ToString());
                    usuario.setFuncao(reader[3].ToString());

                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado com o Id informado!");
                }
            }catch(Exception ex)
            {
                MessageBox.Show("Erro: " + ex.ToString());
            }
            finally
            {
                con.Close();
            }
            return usuario;
        }
        public void deletar(BeansUsuario usu)
        {
            SqlCommand cmd = null;
            SqlConnection con = null;
            string connectionString = @"Server=(localdb)\MSSQLLocalDB;Database=dbcrud;Trusted_Connection=True;";
            String sql = "DELETE FROM USUARIO WHERE ID = '"+usu.getId().ToString()+"'";
            con = new SqlConnection(connectionString);
            cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.Text;
            con.Open();
            try
            {
                int i = cmd.ExecuteNonQuery();
                if (i > 0)
                {
                    MessageBox.Show("Eliminado com sucesso");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.ToString());
            }
            finally
            {
                con.Close();
            }


        }
        public void atualizar(BeansUsuario usu)
        {
            SqlCommand cmd = null;
            SqlConnection con = null;
            string connectionString = @"Server=(localdb)\MSSQLLocalDB;Database=dbcrud;Trusted_Connection=True;";
            String sql = "UPDATE USUARIO SET login = '" +usu.getLogin()+"', senha= '"+ usu.getSenha() +"', funcao ='"+usu.getFuncao()+"' WHERE ID = '" + usu.getId().ToString() + "'";
            con = new SqlConnection(connectionString);
            cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.Text;
            con.Open();
            try
            {
                int i = cmd.ExecuteNonQuery();
                if (i > 0)
                {
                    MessageBox.Show("Atualizado com sucesso");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.ToString());
            }
            finally
            {
                con.Close();
            }


        }

    }
}
